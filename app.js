const express = require("express");
const app = express();
const Kavenegar = require('kavenegar');
const api = Kavenegar.KavenegarApi({
    apikey: process.env.API_KEY
});
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const processSomething = callback => {
  setTimeout(callback, 20000);
}
// Example of data sent from Pakat Automation Webhook
// {
// email: 'test@example.com',
// scenario_id: 1,
// datetime: 1572516474,
// attributes: 
// { LASTNAME: 'هادی',
//   FIRSTNAME: 'فرنود',
//   SMS: '989203007090',
//   'DOUBLE_OPT-IN': '1',
//   EBOOK: true,
//   EMAIL: 'test@example.com' },
// properties: 
// { name: 'hadi',
//   form_id_v2: '5d107139c67c7517823b6396' }
// }

app.post("/hook1", (req, res, next) => {
  processSomething(() => {
    const webhookUrl = req.params.url;
    console.log(req.body);
  if (req.body.attributes.SMS) {
    api.Send({
            message: process.env.MSG1,
            sender: process.env.SENDER,
            receptor: req.body.attributes.SMS
        },
        function(response, status) {
            console.log(status);
        });
  }; //end if
  }); // end of processSomething
  res.status(200).json(JSON.stringify(req.body, null, 2));
}); //end of hook1

app.get("/", (req, res, next) => {
  res.end('<h1>Get Lost!<h1>');
});
app.listen(process.env.PORT || 3000, () =>
  console.log(`Example app listening on port ${process.env.PORT}!`),
);
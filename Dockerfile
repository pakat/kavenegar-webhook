FROM node:10-stretch
COPY . /app
WORKDIR /app
RUN npm i

CMD [ "npm" , "run" ,"start" ]


